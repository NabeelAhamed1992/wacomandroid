package haviktec.wacomepresc.com.finalwacomop.wacom.oppresc;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import haviktec.wacomepresc.com.finalwacomop.R;

public class Patientui extends AppCompatActivity {

    EditText GetName, GetPhoneNumber, GetDob;
    Button Submit, EditData, DisplayData;
    SQLiteDatabase SQLITEDATABASE;
    String Name, PhoneNumber, Date;
    Boolean CheckEditTextEmpty;
    String SQLiteQuery;
    int day, year, month;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patientui);

        GetName = (EditText) findViewById(R.id.editText1);

        GetPhoneNumber = (EditText) findViewById(R.id.editText2);

        GetDob = (EditText) findViewById(R.id.editText3);
        Calendar cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        GetDob.setFocusable(false);

        GetDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(0);

            }
        });

        Submit = (Button) findViewById(R.id.button1);

        EditData = (Button) findViewById(R.id.button2);

        DisplayData = (Button) findViewById(R.id.button3);

        Submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                DBCreate();

                SubmitData2SQLiteDB();

            }
        });

        EditData.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                              Intent intent = new Intent(Patientui.this, EditDataActivity.class);
                startActivity(intent);

            }
        });

        DisplayData.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(Patientui.this, Added_list.class);
                startActivity(intent);

            }
        });
    }

    public void DBCreate() {

        SQLITEDATABASE = openOrCreateDatabase("patient", Context.MODE_PRIVATE, null);

        SQLITEDATABASE.execSQL("CREATE TABLE IF NOT EXISTS patientlist(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR, phone_number VARCHAR, subject VARCHAR);");
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }

    public DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            int month = monthOfYear + 1;
            String formattedMonth = "" + month;
            String formattedDayOfMonth = "" + dayOfMonth;

            if (month < 10) {

                formattedMonth = "0" + month;
            }
            if (dayOfMonth < 10) {

                formattedDayOfMonth = "0" + dayOfMonth;
            }
            // String dateVal = year + "-" + formattedMonth + "-" + formattedDayOfMonth;
            String dateVal = formattedMonth + "-" + formattedDayOfMonth + "-" + year;
            //Date.setText(dateVal);
            GetDob.setText(dateVal);


            // date = dateVal;


        }
    };


    public void   SubmitData2SQLiteDB(){

        Name = GetName.getText().toString();

        PhoneNumber = GetPhoneNumber.getText().toString();

        Date = GetDob.getText().toString();

        CheckEditTextIsEmptyOrNot( Name,PhoneNumber, Date);


        if(CheckEditTextEmpty == true)
        {

            SQLiteQuery = "INSERT INTO patientlist (name,phone_number,subject) VALUES('"+Name+"', '"+PhoneNumber+"', '"+Date+"');";

            SQLITEDATABASE.execSQL(SQLiteQuery);

            Toast.makeText(Patientui.this,"Data Submit Successfully", Toast.LENGTH_LONG).show();

            ClearEditTextAfterDoneTask();

        }
        else {

            Toast.makeText(Patientui.this,"Please Fill All the Fields", Toast.LENGTH_LONG).show();
        }
    }

    public void CheckEditTextIsEmptyOrNot(String Name,String PhoneNumber, String subject ){

        if(TextUtils.isEmpty(Name) || TextUtils.isEmpty(PhoneNumber) || TextUtils.isEmpty(Date)){

            CheckEditTextEmpty = false ;

        }
        else {
            CheckEditTextEmpty = true ;
        }
    }

    public void ClearEditTextAfterDoneTask(){

        GetName.getText().clear();
        GetPhoneNumber.getText().clear();
        GetDob.getText().clear();

    }

}
