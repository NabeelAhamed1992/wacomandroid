package haviktec.wacomepresc.com.finalwacomop.wacom.oppresc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import haviktec.wacomepresc.com.finalwacomop.R;
import haviktec.wacomepresc.com.finalwacomop.wacom.StepperWizardLight;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_fullscreen);
        getSupportActionBar().hide();

        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                finish();
                Intent i = new Intent(FullscreenActivity.this, StepperWizardLight.class);
                startActivity(i);
            }
        };

        handler.postDelayed(r, 3000);

    }


}
