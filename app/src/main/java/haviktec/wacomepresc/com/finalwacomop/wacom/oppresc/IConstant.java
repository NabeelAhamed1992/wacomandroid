package haviktec.wacomepresc.com.finalwacomop.wacom.oppresc;



public interface IConstant {

    String email = "email";
    String password = "password";
    String schoolName = "schoolName";
    String district = "district";
    String loggedInUserType="loggedInUserType";
    String jsonArray = "jsonArray";
    String listSize ="listSize";
    String username="username";
    String user_id="user_id";
    String user_type = "user_type";
    String rememberusername = "rememberusername";
    String rememberpassword = "rememberpassword";


    String user = "user";

    String attachuid="attachuid";
    String attachdocid="attachdocid";
    String ssl = "ssl";
    String baseip = "baseip";
    String compname = "compname";
    public static final String EMPTY_STRING = "";
    public static final String NO_CONNECTION = "Please connect to the network";
    public static final String SELECT_USER = "Please select user";
    public static final int SPLASH_LENGTH_TIME = 5000;
    public static final String REQUEST_TYPE_POST = "POST";
    public static final String REQUEST_TYPE_GET = "GET";

    // Key
    public static final String KEY_CONTENT_TYPE = "Content-Type";
    public static final String KEY_ACCEPT = "Accept";
    public static final String KEY_AUTHENTICATION = "Authorization";
    public static final String KEY_AUTH_VALUE = "Basic Og==";
    public static final String KEY_ERROR = "error";
    public static final String VALUE_CONTENT_TYPE = "application/json";
    public static final String VALUE_URLENCODE = "application/x-www-form-urlencoded";

}
