package haviktec.wacomepresc.com.finalwacomop.wacom.oppresc;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import haviktec.wacomepresc.com.finalwacomop.R;

class listAdapter
    extends RecyclerView.Adapter<listAdapter.MyViewHolder> {

        //FAKE DATA PRO RECYCLER
        ArrayList<Newlist> newlist;

        public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView)view.findViewById(R.id.name);


        }

    }
//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    // send selected contact in callback
//                    listener.onContactSelected(contactListFiltered.get(getAdapterPosition()));
//                }
//            });


    public listAdapter(ArrayList<Newlist> newlist) {

        this.newlist = newlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_row_item, parent, false);

        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Newlist nlist = newlist.get(position);


        holder.title.setText(nlist.getName());

    }



    @Override
    public int getItemCount() {
        return newlist.size();
    }



}


