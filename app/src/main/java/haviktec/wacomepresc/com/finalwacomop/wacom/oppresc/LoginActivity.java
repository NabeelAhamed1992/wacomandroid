package haviktec.wacomepresc.com.finalwacomop.wacom.oppresc;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import haviktec.wacomepresc.com.finalwacomop.R;
import haviktec.wacomepresc.com.finalwacomop.wacom.MainActivity;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    TextView loginTv;
    TextInputEditText usernameEt, passwordEt;
    String userNameSt, passwordSt;
    String user_type;
    ProgressDialog pe;
    private SharedPreferenceManager sharedPrefMgr;
    TextInputLayout userLout, passLout;
    CheckBox rememberCredentials;
    LinearLayout cancellogin;
    public String refreshedToken;
    // private SmoothProgressBar mProgressBar;
    private Interpolator mCurrentInterpolator;
    private float mFactor = 1.0f;
    TextView txt_Error;

    String response = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Login");
        setContentView(R.layout.activity_login);

        sharedPrefMgr = new SharedPreferenceManager(getApplicationContext());
        intiUI();
        sharedPrefMgr.connectDB();
        String preUniqueId = sharedPrefMgr.getString(IConstant.rememberusername);
        String prePassword = sharedPrefMgr.getString(IConstant.rememberpassword);
        System.out.println("pre pass id : " + preUniqueId + " and " + prePassword);
        sharedPrefMgr.closeDB();
        usernameEt.setText(preUniqueId);
        passwordEt.setText(prePassword);

        loginTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userNameSt = usernameEt.getText().toString();
                passwordSt = passwordEt.getText().toString();
                if (userNameSt.length() <= 0) {
                    userLout.setError("Please give input");
                    return;
                } else if (passwordSt.length() <= 0) {
                    passLout.setError("please give input");
                    return;
                }

//
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);

//                validateUserTask task = new validateUserTask();
//                task.execute(new String[]{userNameSt, passwordSt});
        //close on listener
        //    LoginServiceCall();


        if (rememberCredentials.isChecked()) {
            sharedPrefMgr.connectDB();
            sharedPrefMgr.setString(IConstant.rememberusername, userNameSt);
            sharedPrefMgr.setString(IConstant.rememberpassword, passwordSt);
            sharedPrefMgr.closeDB();
        } else {
            sharedPrefMgr.connectDB();
            sharedPrefMgr.setString(IConstant.rememberusername, "");
            sharedPrefMgr.setString(IConstant.rememberpassword, "");
            sharedPrefMgr.closeDB();
        }


            }
        });


        cancellogin.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void intiUI() {
        loginTv = findViewById(R.id.login);
        usernameEt = findViewById(R.id.username);
        passwordEt = findViewById(R.id.password);
        userLout = findViewById(R.id.userError);
        passLout = findViewById(R.id.passError);
        rememberCredentials = findViewById(R.id.rememberLoginDetails);
        cancellogin = findViewById(R.id.loginCancel);
        txt_Error =findViewById(R.id.txtError);
        // mProgressBar = (SmoothProgressBar) findViewById(R.id.progressbar);
    }


    private class validateUserTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("email", params[0] ));
            postParameters.add(new BasicNameValuePair("password", params[1] ));
            Log.e("line===57", String.valueOf(postParameters));
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost("http://35.154.141.219:3000/logindoctor", postParameters);
                Log.e("line===61", response);

                res=response.toString();
                res= res.replaceAll("\\s+","");
                Log.e("line===65", res);

            }
            catch (Exception e) {
                txt_Error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String res) {
            //super.onPostExecute(res);

            try {
                Log.e("res",res);
                JSONObject jo = new JSONObject(res);

                Log.e("jo===", String.valueOf(jo));

                int status = jo.getInt("status");

                if (status==1) {

                    Intent i = new Intent(LoginActivity.this, DashBoardActivity.class);
                    startActivity(i);
                }
                else{
                    txt_Error.setText("Sorry!! Incorrect Username or Password");
                }} catch (JSONException e1) {
                e1.printStackTrace();
            }
        }}}
