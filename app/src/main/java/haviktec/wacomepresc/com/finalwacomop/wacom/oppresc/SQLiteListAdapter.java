package haviktec.wacomepresc.com.finalwacomop.wacom.oppresc;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import haviktec.wacomepresc.com.finalwacomop.R;
import haviktec.wacomepresc.com.finalwacomop.wacom.FileTransferActivity;

public class SQLiteListAdapter extends RecyclerView.Adapter<SQLiteListAdapter.ViewHolder> {

    // Because RecyclerView.Adapter in its current form doesn't natively
    // support cursors, we wrap a CursorAdapter that will do all the job
    // for us.
    CursorAdapter mCursorAdapter;


    Context context;
    ArrayList<String> userID;
    ArrayList<String> UserName;
    ArrayList<String> User_PhoneNumber;
    ArrayList<String> UserBob;

   AdapterListener listener;
    public SQLiteListAdapter(
            Context context2,
            ArrayList<String> id,
            ArrayList<String> name,
            ArrayList<String> phone,
            ArrayList<String> Date
    ) {

        this.context = context2;
        this.userID = id;
        this.UserName = name;
        this.User_PhoneNumber = phone;
        this.UserBob = Date;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textviewid;
        TextView textviewname;
        TextView desc;
        TextView textviewdob;
        Button prescription;
        ItemClickListener itemClickListener;

        public ViewHolder(View itemView) {
            super(itemView);

            textviewname = (TextView) itemView.findViewById(R.id.name);
            desc = (TextView) itemView.findViewById(R.id.description);
            prescription = itemView.findViewById(R.id.prescription);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v,getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic)
        {
            this.itemClickListener=ic;

        }
    }


    @Override

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //VIEW OBJ
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_people_chat, null);

        //HOLDER
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // Passing the binding operation to cursor loader


        holder.desc.setText(String.valueOf("Id :"+userID.get(position)+"\n"+UserBob.get(position)+"\n"+User_PhoneNumber.get(position)));
        holder.textviewname.setText(UserName.get(position));
        holder.prescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(context, FileTransferActivity.class);
                Bundle b = new Bundle();
                b.putString("name", UserName.get(position));
                b.putString("patient_id",userID.get(position));
                intent.putExtras(b);
                context.startActivity(intent);

            }
        });

        Log.e("id============4", UserBob.get(position));
       holder.setItemClickListener(new ItemClickListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    Intent intent = new Intent(context, prescription.class);
                    Bundle b = new Bundle();

                    //Inserts a String value into the mapping of this Bundle
                    b.putString("Name", UserName.get(pos));
                    b.putString("Phone",User_PhoneNumber.get(pos));

                    //Add the bundle to the intent.
                    intent.putExtras(b);


                    context.startActivity(intent);

                    Snackbar.make(v, UserName.get(pos), Snackbar.LENGTH_SHORT).show();
                }
            });

        }



    @Override
    public int getItemCount() {
        return UserName.size();
    }

    public class AdapterListener {
    }

    public interface ItemClickListener {

        void onItemClick(View v, int pos);
    }
}
