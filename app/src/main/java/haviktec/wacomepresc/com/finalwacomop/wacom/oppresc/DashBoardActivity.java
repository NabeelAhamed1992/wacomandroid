package haviktec.wacomepresc.com.finalwacomop.wacom.oppresc;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import haviktec.wacomepresc.com.finalwacomop.R;

public class DashBoardActivity extends AppCompatActivity {
    LinearLayout healthreportLayout, attachLayout, doccsLayout, doccsEHRLayout;
    private ViewPager simpleViewFlipper;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Context mContext;
    private Activity mActivity;
 private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        // healthreportLayout.setVisibility(View.VISIBLE);
        CollapsingToolbarLayout toolbarLayout = findViewById(R.id.toolbar_layout);
        final Typeface tf = Typeface.createFromAsset(getAssets(), "raleway_regular.ttf");
        toolbarLayout.setCollapsedTitleTypeface(tf);
        toolbarLayout.setExpandedTitleTypeface(tf);
      //  mToolbar = (Toolbar) findViewById(R.id.toolbar);

        // Set a title for toolbar
       // mToolbar.setTitle("Android Options Menu CheckBox");
       // mToolbar.setTitleTextColor(Color.WHITE);

        // Set support actionbar with toolbar
     //   setSupportActionBar(mToolbar);

        // Change the toolbar background color
       // mToolbar.setBackgroundColor(Color.parseColor("#FFF18BB7"));

        initIds();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DashBoardActivity.this, Patientui.class);
                startActivity(i);
            }
        });
//


        android.support.v7.app.AlertDialog.Builder mBuilder = new android.support.v7.app.AlertDialog.Builder(DashBoardActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_app_updates, null);
       final CheckBox mCheckBox = mView.findViewById(R.id.checkBox);

        mBuilder.setView(mView);
        mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
if(mCheckBox.isChecked())
{
    checkfun();
}
else {
    Uncheckfun() ;
}
                dialogInterface.dismiss();

            }
        });
        android.support.v7.app.AlertDialog mDialog = mBuilder.create();
        mDialog.show();
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    storeDialogStatus(true);

                } else {

                    storeDialogStatus(false);

                }
            }
        });

        if (getDialogStatus()) {
            mDialog.hide();
        } else {
            mDialog.show();
        }



        healthreportLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Patientlist = new Intent(getApplicationContext(), Patientlist.class);
                startActivity(Patientlist);
            }
        });
        attachLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  selectImage();
                Intent dashtoNormal = new Intent(getApplicationContext(), Attachment.class);
                startActivity(dashtoNormal);
            }
        });
        doccsEHRLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dashToChronic = new Intent(getApplicationContext(), doccsehr.class);
                startActivity(dashToChronic);
            }
        });
        doccsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dashToChronic = new Intent(getApplicationContext(), doccs.class);
                startActivity(dashToChronic);
            }
        });

    }

    private void Uncheckfun() {
        Intent i = new Intent(DashBoardActivity.this, Added_list.class);
        startActivity(i);
    }

    private void checkfun() {

                Intent Patientlist = new Intent(getApplicationContext(), Patientlist.class);
                startActivity(Patientlist);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_options_menu, menu);
        SharedPreferences settings = getSharedPreferences("settings", 0);
        boolean isChecked = settings.getBoolean("checkbox", false);
        MenuItem item = menu.findItem(R.id.action_check);
        item.setChecked(isChecked);
        return true;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu){
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.toolbar_options_menu, menu);
//        return true;
//    }
@Override
public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.action_check) {
        item.setChecked(!item.isChecked());
        SharedPreferences settings = getSharedPreferences("settings", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("checkbox", item.isChecked());
        editor.commit();
        return true;
    }
    return super.onOptionsItemSelected(item);
}


    private void storeDialogStatus(boolean isChecked)
    {
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean("item", isChecked);
        mEditor.apply();
    }

    private boolean getDialogStatus()
    {
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        return mSharedPreferences.getBoolean("item", false);
    }


    private void initIds()
    {

        healthreportLayout = findViewById(R.id.prescription);
        attachLayout = findViewById(R.id.attach);
        doccsLayout = findViewById(R.id.docs);
        doccsEHRLayout = findViewById(R.id.detail);
//
    }
}